
// Abre / fecha menu
var menuisopen = false;
$('#menu-button').on('click', function(event) {
	event.preventDefault();

	if (menuisopen) {
		$('#page').animate({left:'0'});
		$('#menu-button').animate({opacity:'0'}, function(){
			$(this).attr('class', 'open');
			$(this).animate({opacity:'1'});
		});
		menuisopen = false;
	} else {
		$('#page').animate({left:'300px'});
		$('#menu-button').animate({opacity:'0'}, function(){
			$(this).attr('class', 'close');
			$(this).animate({opacity:'1'});
		});
		menuisopen = true;
	}
});

$(document).ready(function() {
	// Carrrossel principal
	$('.carousel_stage2').slick({});

	$('.carousel_stage').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		asNavFor: '.carousel_nav'
	});
	$('.carousel_nav').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		asNavFor: '.carousel_stage'
	});

	// Carrossel de Marcas Parceiras
	$('.parceiras_carousel').slick({
		slidesToShow: 4,
		slidesToScroll: 4,
		infinite: true,
	})
});