<?php
/*
Template Name: Categorias
*/
?>
<section class="medium container nossosprodutos">
   <h2>Nossos produtos</h2>
   <ul>
      <li class="comunicacao" onclick="javascript:location.href='produto?categoria=comunicação&cat_id=1';" style="cursor:pointer;">
        <h3>Comunicação</h3>
      </li>
      <li class="navegacao" onclick="javascript:location.href='produto?categoria=navegação&cat_id=2';" style="cursor:pointer;">
        <h3>Navegação</h3>
      </li>
      <li class="seguranca" onclick="javascript:location.href='produto?categoria=segurança&cat_id=3';" style="cursor:pointer;">
        <h3>Segurança</h3>
      </li>
   </ul>
</section>