<?php
/**
 * Setup theme functions for Minamaze.
 *
 * @package ThinkUpThemes
 */

// Setup content width
if ( ! isset( $content_width ) )
	$content_width = 960;


/* Local */
$contact_form_Id = "490";
$contact_form_Id_us = "577";
$contact_form_Id_es = "1459";
$contact_news_Id = "520";
$contact_news_Id_us = "576";
$contact_news_Id_es = "1460";

/* Homolog */
/*
$contact_form_Id = "490";
$contact_form_Id_us = "577";
$contact_form_Id_es = "1629";
$contact_news_Id = "520";
$contact_news_Id_us = "576";
$contact_news_Id_es = "1630";
*/

/* Prod */
/*
$contact_form_Id = "490";
$contact_form_Id_us = "577";
$contact_form_Id_es = "1629";
$contact_news_Id = "520";
$contact_news_Id_us = "576";
$contact_news_Id_es = "1630";
*/

// ----------------------------------------------------------------------------------
//	Add Theme Options Panel & Assign Variable Values
// ----------------------------------------------------------------------------------

	// Add Redux Framework - Credits attributable to http://reduxframework.com/
	require_once (get_template_directory() . '/admin/main/framework.php');

	// Add Theme Options Features.
	require_once( get_template_directory() . '/admin/main/options/00.theme-setup.php' );
	require_once( get_template_directory() . '/admin/main/options/00.variables.php' );
	require_once( get_template_directory() . '/admin/main/options/01.general-settings.php' );
	require_once( get_template_directory() . '/admin/main/options/02.homepage.php' );
	require_once( get_template_directory() . '/admin/main/options/03.header.php' );
	require_once( get_template_directory() . '/admin/main/options/04.footer.php' );
	require_once( get_template_directory() . '/admin/main/options/05.blog.php' );
	require_once( get_template_directory() . '/admin/main/options/08.special-pages.php' );

	// Add widget features.
//	include_once( get_template_directory() . '/lib/widgets/categories.php' );
//	include_once( get_template_directory() . '/lib/widgets/popularposts.php' );
//	include_once( get_template_directory() . '/lib/widgets/recentposts.php' );
//	include_once( get_template_directory() . '/lib/widgets/searchfield.php' );
//	include_once( get_template_directory() . '/lib/widgets/tagscloud.php' );

// ----------------------------------------------------------------------------------
//	Assign Theme Specific Functions
// ----------------------------------------------------------------------------------


// Setup theme features, register menus and scripts.
if ( ! function_exists( 'thinkup_themesetup' ) ) {

	function thinkup_themesetup() {

		// Load required files
		require_once ( get_template_directory() . '/lib/functions/extras.php' );
		require_once ( get_template_directory() . '/lib/functions/template-tags.php' );

		// Make theme translation ready.
		load_theme_textdomain( 'lan-thinkupthemes', get_template_directory() . '/languages' );

		// Add default theme functions.
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'custom-background' );
		add_theme_support( 'title-tag' );

		// Add support for custom header
		$args = apply_filters( 'custom-header', array( 'height' => 200, 'width'  => 1600 ) );
		add_theme_support( 'custom-header', $args );

		// Add WooCommerce functions.
		add_theme_support( 'woocommerce' );

		// Register theme menu's.
		register_nav_menus( array( 'pre_header_menu' => 'Pre Header Menu', ) );
		register_nav_menus( array( 'header_menu' => 'Primary Header Menu', ) );
		register_nav_menus( array( 'sub_footer_menu' => 'Footer Menu', ) );
                
                // Adiciona tamanho de imagem pro carrossel de marcas parceiras.
                add_image_size( 'parceiras-thumb', 100, 80 ); // Imagem do carrossel de parceiras
	}
}
add_action( 'after_setup_theme', 'thinkup_themesetup' );


// ----------------------------------------------------------------------------------
//	Register Front-End Styles And Scripts
// ----------------------------------------------------------------------------------

function thinkup_frontscripts() {

	// Add jQuery library.
	wp_enqueue_script('jquery');

	// Register theme stylesheets.
	wp_register_style( 'style', get_stylesheet_uri(), '', '1.1.9' );
	wp_register_style( 'shortcodes', get_template_directory_uri() . '/styles/style-shortcodes.css', '', '1.1' );
	wp_register_style( 'responsive', get_template_directory_uri() . '/styles/style-responsive.css', '', '1.1' );
	wp_register_style( 'sidebarleft', get_template_directory_uri() . '/styles/layouts/thinkup-left-sidebar.css', '', '1.1' );
	wp_register_style( 'sidebarright', get_template_directory_uri() . '/styles/layouts/thinkup-right-sidebar.css', '', '1.1' );
	wp_register_style( 'bootstrap', get_template_directory_uri() . '/lib/extentions/bootstrap/css/bootstrap.min.css', '', '2.3.2' );
	wp_register_style( 'prettyPhoto', get_template_directory_uri().'/lib/extentions/prettyPhoto/css/prettyPhoto.css', '', '3.1.6' );

	// Register Font Packages.
	wp_register_style( 'font-awesome-min', get_template_directory_uri() . '/lib/extentions/font-awesome/css/font-awesome.min.css', '', '3.2.1' );
	wp_register_style( 'font-awesome-cdn', get_template_directory_uri() . '/lib/extentions/font-awesome-4.2.0/css/font-awesome.min.css', '', '4.2.0' );
	wp_register_style( 'dashicons-css', get_template_directory_uri() . '/lib/extentions/dashicons/css/dashicons.css', '', '2.0' );

	// Register theme scripts.
	wp_register_script( 'frontend', get_template_directory_uri() . '/lib/scripts/main-frontend.js', array( 'jquery' ), '1.1', true );
	wp_register_script( 'modernizr', get_template_directory_uri() . '/lib/scripts/modernizr.js', array( 'jquery' ), '', true );
	wp_register_script( 'retina', get_template_directory_uri() . '/lib/scripts/retina.js', array( 'jquery' ), '', true );
	wp_register_script( 'bootstrap', get_template_directory_uri() . '/lib/extentions/bootstrap/js/bootstrap.js', array( 'jquery' ), '2.3.2', true );
	wp_register_script( 'prettyPhoto', ( get_template_directory_uri().'/lib/extentions/prettyPhoto/js/jquery.prettyPhoto.js' ), array( 'jquery' ), '3.1.6', true );

		// Add Font Packages
		wp_enqueue_style( 'font-awesome-min' );
		wp_enqueue_style( 'font-awesome-cdn' );
		wp_enqueue_style( 'dashicons-css' );

		// Add theme stylesheets
		wp_enqueue_style( 'bootstrap' );
		wp_enqueue_style( 'prettyPhoto' );
		wp_enqueue_style( 'style' );
		wp_enqueue_style( 'shortcodes' );

		// Add theme scripts
		wp_enqueue_script( 'prettyPhoto' );
		wp_enqueue_script( 'frontend' );
		wp_enqueue_script( 'bootstrap' );
		wp_enqueue_script( 'modernizr' );

		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}

		// Add ThinkUpSlider scripts
		if ( is_front_page() or thinkup_check_ishome() ) {
			wp_enqueue_script( 'thinkupslider', get_template_directory_uri() . '/lib/scripts/plugins/ResponsiveSlides/responsiveslides.min.js', array( 'jquery' ), '1.54' );
		wp_enqueue_script( 'thinkupslider-call', get_template_directory_uri() . '/lib/scripts/plugins/ResponsiveSlides/responsiveslides-call.js', array( 'jquery' ) );
		}
}
add_action( 'wp_enqueue_scripts', 'thinkup_frontscripts', 10 );


// ----------------------------------------------------------------------------------
//	Register Back-End Styles And Scripts
// ----------------------------------------------------------------------------------

function thinkup_adminscripts() {

	// Register theme stylesheets.
	wp_register_style( 'backend', get_template_directory_uri() . '/styles/backend/style-backend.css', '', 1.3 );

	// Register theme scripts.
	wp_register_script( 'backend', get_template_directory_uri() . '/lib/scripts/main-backend.js', array( 'jquery' ), '1.1' );

		// Add theme stylesheets
		wp_enqueue_style( 'backend' );

		// Add theme scripts
		wp_enqueue_script( 'backend' );
}
add_action( 'admin_enqueue_scripts', 'thinkup_adminscripts' );


//----------------------------------------------------------------------------------
//	Register Shortcodes Styles And Scripts
//----------------------------------------------------------------------------------

function thinkup_shortcodescripts() {

	// Register shortcode scripts
	wp_register_script( 'thinkupslider', get_template_directory_uri() . '/lib/scripts/plugins/ResponsiveSlides/responsiveslides.min.js', array( 'jquery' ), '1.54', 'true' );
	wp_register_script( 'thinkupslider-call', get_template_directory_uri() . '/lib/scripts/plugins/ResponsiveSlides/responsiveslides-call.js', array( 'jquery' ), '', 'true' );

		// Add shortcode scripts
		wp_enqueue_script( 'thinkupslider' );
		wp_enqueue_script( 'thinkupslider-call' );
}
add_action( 'wp_enqueue_scripts', 'thinkup_shortcodescripts', 10 );


// ----------------------------------------------------------------------------------
//	Register Theme Widgets
// ----------------------------------------------------------------------------------

function thinkup_widgets_init() {
	register_sidebar( array(
		'name' => 'Sidebar',
		'id' => 'sidebar-1',
		'before_widget' => '<aside class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

    register_sidebar( array(
        'name' => 'Footer Widget Area 1',
        'id' => 'footer-w1',
        'before_widget' => '<aside class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="footer-widget-title"><span>',
        'after_title' => '</span></h3>',
    ) );

    register_sidebar( array(
        'name' => 'Footer Widget Area 2',
        'id' => 'footer-w2',
        'before_widget' => '<aside class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="footer-widget-title"><span>',
        'after_title' => '</span></h3>',
    ) );

    register_sidebar( array(
        'name' => 'Footer Widget Area 3',
        'id' => 'footer-w3',
        'before_widget' => '<aside class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="footer-widget-title"><span>',
        'after_title' => '</span></h3>',
    ) );

    register_sidebar( array(
        'name' => 'Footer Widget Area 4',
        'id' => 'footer-w4',
        'before_widget' => '<aside class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="footer-widget-title"><span>',
        'after_title' => '</span></h3>',
    ) );

    register_sidebar( array(
        'name' => 'Footer Widget Area 5',
        'id' => 'footer-w5',
        'before_widget' => '<aside class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="footer-widget-title"><span>',
        'after_title' => '</span></h3>',
    ) );

    register_sidebar( array(
        'name' => 'Footer Widget Area 6',
        'id' => 'footer-w6',
        'before_widget' => '<aside class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="footer-widget-title"><span>',
        'after_title' => '</span></h3>',
    ) );
 }
add_action( 'widgets_init', 'thinkup_widgets_init' );


function arphabet_widgets_init() {
	register_sidebar(array(
		'name' => 'Idiomas',
		'id' => 'idiomas',
		'before_widget' => '<div>',
		'after_widget' => '</div>',
	));
}
add_action('widgets_init', 'arphabet_widgets_init');


function search_widgets_init() {
	register_sidebar(array(
		'name' => 'Pesquisa',
		'id' => 'pesquisa',
		'before_widget' => '<div>',
		'after_widget' => '</div>',
		'before_title' => '<h2>',
		'after_title' => '</h2>',
	));
}
add_action('widgets_init', 'search_widgets_init');


//load_textdomain('minamaze');
load_theme_textdomain( 'minamaze', get_template_directory() . '/languages' );

/** Adicionando custom type **/
add_action('init', 'projetos_function');

function projetos_function(){
	$labels = array(
		'name' => "Projetos",
		'menu_name' => "Projetos",
		'name_admin_bar' => "Projeto",
		'add_new' => "Cadastrar projeto",
		'add_new_item' => "Novo projeto",
		'edit_item' => "Editar projeto",
		'view_item' => "Ver projeto no site",
		'search_items' => "Procurar projeto",
		'not_found' => "Registro não encontrado",
		);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'has_archive' => true,
		'rewrite' => array('slug' => 'projetos'),
		'supports' => array('title'),
		);

	register_post_type('projeto', $args);
}

/** Adicionando custom type Produtos **/
add_action('init', 'produtos_function');

function produtos_function(){
	$labels = array(
		'name' => "Produtos",
		'menu_name' => "Produtos",
		'name_admin_bar' => "Produtos",
		'add_new' => "Cadastrar produto",
		'add_new_item' => "Novo produto",
		'edit_item' => "Editar produto",
		'view_item' => "Ver produto no site",
		'search_items' => "Procurar produto",
		'not_found' => "Registro não encontrado",
		);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'has_archive' => true,
		'rewrite' => array('slug' => 'produtos'),
		'supports' => array('title'),
		);
	register_post_type('produto', $args);
}

add_action('init', 'parceiros_function');

function parceiros_function(){
	$labels = array(
		'name' => "Parceiros",
		'menu_name' => "Parceiros",
		'name_admin_bar' => "Parceiros",
		'add_new' => "Cadastrar parceiro",
		'add_new_item' => "Novo parceiro",
		'edit_item' => "Editar parceiro",
		'view_item' => "Ver parceiro no site",
		'search_items' => "Procurar parceiro",
		'not_found' => "Registro não encontrado",
		);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'has_archive' => true,
		'rewrite' => array('slug' => 'parceiro'),
		'supports' => array('title'),
		);
	register_post_type('parceiro', $args);
}

add_action('init', 'tipo_produto_function');

function tipo_produto_function(){
	$labels = array(
		'name' => "Tipo Produto",
		'menu_name' => "Tipo Produto",
		'name_admin_bar' => "Tipo Produto",
		'add_new' => "Cadastrar Tipo de Produto",
		'add_new_item' => "Novo Tipo de Produto",
		'edit_item' => "Editar Tipo de Produto",
		'view_item' => "Ver Tipo de Produto no site",
		'search_items' => "Procurar Tipo de Produto",
		'not_found' => "Registro não encontrado",
		);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'has_archive' => true,
		'rewrite' => array('slug' => 'tipo_produto'),
		'supports' => array('title'),
		);
	register_post_type('tipo_produto', $args);
}

add_action('init', 'marca_produto_function');

function marca_produto_function(){
	$labels = array(
		'name' => "Marca Produto",
		'menu_name' => "Marca Produto",
		'name_admin_bar' => "Marca Produto",
		'add_new' => "Cadastrar Marca do Produto",
		'add_new_item' => "Novo Marca do Produto",
		'edit_item' => "Editar Marca do Produto",
		'view_item' => "Ver Marca do Produto no site",
		'search_items' => "Procurar Marca do Produto",
		'not_found' => "Registro não encontrado",
		);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'has_archive' => true,
		'rewrite' => array('slug' => 'Marca_produto'),
		'supports' => array('title'),
		);
	register_post_type('marca_produto', $args);
}


add_action('init', 'cliente_projeto_function');

function cliente_projeto_function(){
	$labels = array(
		'name' => "Cliente",
		'menu_name' => "Cliente",
		'name_admin_bar' => "Cliente",
		'add_new' => "Cadastrar Cliente",
		'add_new_item' => "Novo Cliente",
		'edit_item' => "Editar Cliente",
		'view_item' => "Ver Cliente no site",
		'search_items' => "Procurar Cliente",
		'not_found' => "Registro não encontrado",
		);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'has_archive' => true,
		'rewrite' => array('slug' => 'Cliente_projeto'),
		'supports' => array('title'),
		);
	register_post_type('cliente_projeto', $args);
}
/** Fim Custom Post Types **/

function listagem_parceiros(){
?>
    <section id="marcas_parceiras">
        <div class="container">
            <h2><?php _e('MARCAS PARCEIRAS', 'minamaze'); ?></h2>
            <ul class="parceiras_carousel">
		<?php
                $loop = new WP_Query( array( 'post_type' => 'parceiro' ) );
//                echo '<pre>';
//                print_r($loop->post_count);
//                echo '</pre>';
                while ( $loop->have_posts()) : $loop->the_post(); ?>
                    <li class="parceiras_slide" style="display: -webkit-box;">
                        <?php $imagem = get_field('imagem'); ?>
                        <?php echo wp_get_attachment_image( $imagem['id'], 'parceiras-thumb' ); ?>
<!--                        <img style="border-radius:50%; max-height:140px; max-width:140px;" src="<?php// the_field('imagem') ?>">-->
                    </li>
                <?php endwhile; ?>
            </ul>
        </div>
    </section>
<?php
}

add_filter('excerpt_length', 'my_excerpt_length');
function my_excerpt_length($len) { return 20; }
function excerpt_readmore($more) {
    return '...';
}
add_filter('excerpt_more', 'excerpt_readmore');

function remove_campos_personalizados() {

    // make sure we're on an admin screen and `post` is set
    //if( !is_admin() && !isset( $_GET['post'] ) )
    //    return;
	if( $_GET['post'] != 8 ){
		remove_meta_box( 'mf_1' , 'page' , 'normal' );	
	}
    
   // remove_meta_box( 'banner_de_destaque' , 'page' , 'normal' );
    //remove_meta_box( 'postcustom' , 'page' , 'normal' );

    //$post = $_GET['post'];
    //remove_meta_box( 'mf_1', 'page', 'normal' );

    //if( $_GET['post'] == 8 ): // editing page ID 99
    //    remove_meta_box( 'banner_de_destaque', 'page', 'normal' );
    //endif;

}

add_action( 'admin_menu', 'remove_campos_personalizados' );

add_action( 'admin_head', 'remove_campos_personalizados' );

//load_textdomain('minamaze');
load_theme_textdomain( 'minamaze', get_template_directory() . '/languages' );

function parametros_projetos($qtdePagina, $ehPaginacao) {

	$paged = get_query_var('paged') ? get_query_var('paged') : 1;
	$cliente = '';
	$ordernar = 'DESC';

	if ($ehPaginacao) {

		$cliente = htmlspecialchars($_GET["cliente"]);
		$ordernar_temp = htmlspecialchars($_GET["ordernar"]);

		if ($cliente > '0'){
		   	$cliente = array(
					'key' => 'cliente',
					'value' => $cliente,
					'compare' => '=',
				);
		}

		if (!empty($ordernar_temp)) {
			if ($ordernar == 0){
				$ordernar = 'ASC';
			}
		}
	}

	$args = array(
   			'post_type' => 'projeto',
   			'posts_per_page' => $qtdePagina,
   			'paged' => $paged,
   			'meta_query' => array($cliente),
   			'meta_key' => 'ano',
   			'orderby' => 'meta_value_num',
   			'order'=> $ordernar
		   	);
	return $args;
}

function listagem_projetos($qtdePagina, $ehPaginacao){
?>
<?php 
	$paged = get_query_var('paged') ? get_query_var('paged') : 1; 
	
	//echo 'pagina = ' . $paged; 
	
	$loop = new WP_Query(array( 'post_type' => 'projeto', 'posts_per_page' => $qtdePagina, 'paged' => $paged ));
	echo '<ul>';
	while ( $loop->have_posts() ) : $loop->the_post(); ?>
		<li>
			<header>
				<h3><?php the_title(); get_post_meta($post->ID, '_equipamento_projeto', true); ?></h3>
			</header>
			<div class="info">
				<?php
				$metadados = get_post_meta(get_the_ID()); 			    	 
				echo '<span class="year">' . $metadados['_ano_projeto'][0] . '</span>';
				echo '<div class="client"><p>' . $metadados['_realizador_projeto'][0] . '</p></div>';
				$texto = __('Equipamentos', 'minamaze');
				get_post_meta($post->ID, '_equipamento_projeto', true);
				echo '<h4>' . $texto . '</h4>';
				echo '<p>' . $metadados['_equipamento_projeto'][0] . '</p>';
				$texto = __('Estaleiro', 'minamaze');
				echo '<h4>' . $texto . '</h4>';
				echo '<p>' . $metadados['_estaleiro_projeto'][0] . '</p>';
				?>
			</div>
		</li> 
	<?php 
		endwhile;
		echo '</ul>'; 
//		if ($ehPaginacao) {
//		wp_pagenavi(array( 'query' => $loop ));
//		}
	?>
<?php
}

add_action('plugins_loaded', 'qtranslate_monkey_patch', 3);
function qtranslate_monkey_patch() {
	global $q_config;
	if (strpos($q_config['js']['qtrans_hook_on_tinyMCE'], 'ed.editorId.match(/^qtrans_/)') === false) {
		$q_config['js']['qtrans_hook_on_tinyMCE'] = preg_replace("/(qtrans_save\(switchEditors\.pre_wpautop\(o\.content\)\);)/", "if (ed.editorId.match(/^qtrans_/)) \$1", $q_config['js']['qtrans_hook_on_tinyMCE']);
	}
}