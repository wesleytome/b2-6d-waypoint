<?php
/*
Template Name: O que fazemos
*/
?>

<?php get_header();?>
    <?php while( have_posts() ): the_post(); ?>
        <main class="oquefazemos interna">
            
            <section class="titulo" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/header-oquefazemos.png');">
                <div class="medium container">
                    <div class="title-box">
                        <h1><?php _e('O que fazemos', 'minamaze'); ?></h1>
                    </div>
                </div>
            </section>
            
            <article class="oquefazemos">
                
                <section class="medium container">
                    <?php echo  the_field('o_que_fazemos');  ?>
                </section>
                
                <section class="projetos">
                    <div class="container lista">
                        <h2><a id="o_que_fazemos_projetos"><?php _e('Projetos', 'minamaze'); ?></a></h2>
                        <?php get_template_part( 'projeto' ); ?>
                        <a id="o_que_fazemos_btn_projetos" class="botaoProjeto"><?php _e('Projetos', 'minamaze'); ?></a>
                    </div>
                </section>
                
                <section class="medium container">
                    <a id="o_que_fazemos_onde_estamos" class="pagelink leftlink" style="background: #fff;">
                        <img class="img-responsive" src='<?php echo get_template_directory_uri(); ?>/img/pagelink-ondeestamos.png' alt="Onde estamos">
                        <h3><?php _e('ONDE', 'minamaze'); ?><br><?php _e('ESTAMOS', 'minamaze'); ?></h3>
                    </a>
                    <a id="o_que_fazemos_quem_somos" class="pagelink rightlink" style="background: #fff;">
                        <img class="img-responsive" src='<?php echo get_template_directory_uri(); ?>/img/pagelink-quemsomos.png' alt="Quem somos">
                        <h3><?php _e('QUEM', 'minamaze'); ?><br><?php _e('SOMOS', 'minamaze'); ?></h3>
                    </a>
                </section>
                
            </article>
        </main>
    <?php endwhile; ?>
<?php get_footer(); ?>