<?php
/*
Template Name: Projetos
*/
?>

<?php get_header(); ?>

<main class="projetos interna">
    
    <section class="titulo" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/header-projetos.png');">
        <div class="medium container">
            <div class="title-box">
                <h1><?php echo _e('Projetos', 'minamaze'); ?></h1>
            </div>
        </div>
    </section>
    
    <section class="filtros container">
        <form action="">
            <select name="drpProjetoCliente" id="drpProjetoCliente">
                <option value="0"><?php _e('Todos os clientes', 'minamaze'); ?></option>;
                <?php $loop = new WP_Query( array( 'post_type' => 'cliente_projeto') );
                while ( $loop->have_posts()) : $loop->the_post(); ?>
                    <option value="<?php echo get_the_ID()?>"><?php the_title(); ?></option>
                <?php endwhile; ?>
            </select>
            <select name="drpProjetoOrdernar" id="drpProjetoOrdernar">
                <option value="0"><?php _e('Ordenar por mais recentes', 'minamaze'); ?></option>;
                <option value="1"><?php _e('Ordenar por menos recente', 'minamaze'); ?></option>;
            </select>
        </form>
    </section>
    
    <section class="projetos">
        <div class="container lista">
            <?php
            $args = parametros_projetos(16, true);
            
            echo '<ul style="border-bottom: 1px solid #d8d8d8; margin-bottom: 30px;">';
            
            $loop = new WP_Query($args);
            while ( $loop->have_posts() ) : $loop->the_post(); ?>
            <li>
                <header>
                    <h3><?php the_title(); ?></h3>
                </header>
                <div class="info">
                    <?php
                    $ano = get_field('ano');
                    echo '<span class="year">' . $ano . '</span>';
                    $cliente_id = get_post_meta(get_the_ID(), 'cliente', true);
                    echo '<div class="client"><p>' . get_the_title( $cliente_id ) . '</p></div>';
                    
                    $texto = __('Equipamentos', 'minamaze');
                    echo '<h4>' . $texto . '</h4>';
                    echo '<p style="line-height:1.5;">' . get_field('equipamento') . '</p>';
                    $texto = __('Estaleiro', 'minamaze');
                    echo '<h4 style="margin-top: 15px;">' . $texto . '</h4>';
                    echo '<p>' . get_field('estaleiro') . '</p>';
                    ?>
                </div>
            </li>
            <?php
            endwhile;
            echo '</ul><div style="text-align:center;">';
            wp_pagenavi(array( 'query' => $loop ));
            echo '</div>'
            ?>
            
            <section class="medium container links-produtos" style="display: none;">
                <a id="quem_somos_onde_estamos" class="pagelink leftlink" style="background: #fff;">
                    <img class="img-responsive" src='<?php echo get_template_directory_uri(); ?>/img/pagelink-ondeestamos.png' alt="Onde estamos">
                    <h3><?php _e('ONDE', 'minamaze'); ?><br><?php _e('ESTAMOS', 'minamaze'); ?></h3>
                </a>
                <a id="quem_somos_o_que_fazemos" class="pagelink rightlink" style="background: #fff;">
                    <img class="img-responsive" src='<?php echo get_template_directory_uri(); ?>/img/pagelink-oquefazemos.png' alt="O que fazemos">
                    <h3><?php _e('O QUE', 'minamaze'); ?><br><?php _e('FAZEMOS', 'minamaze'); ?></h3>
                </a>
            </section>
            
	  </div>
    </section>
    
</main>

<?php
get_footer();
?>

