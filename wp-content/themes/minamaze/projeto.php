<?php
    $args = parametros_projetos(8, false);
    echo '<ul>';
    
    $loop = new WP_Query($args);
    while ( $loop->have_posts() ) : $loop->the_post(); ?>
        <li>
            <header>
                <h3><?php the_title(); ?></h3>
            </header>
            <div class="info">
                <?php
                $ano = get_field('ano');
                echo '<span class="year">' . $ano . '</span>';
                $cliente_id = get_post_meta(get_the_ID(), 'cliente', true);
                echo '<div class="client"><p>' . get_the_title( $cliente_id ) . '</p></div>';
                
                $tit_equip = __('Equipamentos', 'minamaze');
                echo '<h4>' . $tit_equip . '</h4>';
                echo '<p style="line-height:1.5;">' . get_field('equipamento') . '</p>';
                $tit_esta = __('Estaleiro', 'minamaze');
                echo '<h4>' . $tit_esta . '</h4>';
                echo '<p>' . get_field('estaleiro') . '</p>';
                ?>
            </div>
        </li>
        <?php
    endwhile;
    echo '</ul>';
?>