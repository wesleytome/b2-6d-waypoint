<?php
/*
Template Name: Produtos
*/
?>

<?php get_header(); ?>

<main class="produtos interna">
    
    <section class="titulo" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/header-produtos.png');">
        <div class="medium container">
            <div class="title-box">
                <h1><?php echo _e('Produtos', 'minamaze'); ?></h1>
            </div>
        </div>
    </section>
    
    <?php get_template_part('produtos_filtro'); ?>
    
    <article class="produtos">
        <div class="container">
            <ul>
                <?php
                $paged = get_query_var('paged') ? get_query_var('paged') : 1;
                $tipo = '';
                $marca = '';
                $categoria = '';
                $cat_id = '';
                $nome = '';
                
                if (isset($_GET['dropCategoria'])) {
                    $categoria = htmlspecialchars($_GET["dropCategoria"]);
                    $cat_id = htmlspecialchars($_GET["cat_id"]);
                    $tipo = htmlspecialchars($_GET["drpTipo"]);
                    $marca = htmlspecialchars($_GET["drpMarca"]);
                    $nome = htmlspecialchars($_GET["nome"]);
                    
                    if ($cat_id == '0'){
                        
                    } else {
                        if (isset($_GET['lang'])) {
                            switch ($cat_id) {
                                case '2':
                                    $categoria = 'Navegação';
                                    break;									
                                case '3':
                                    $categoria = 'Segurança';
                                    break;
                                default:
                                    $categoria = 'Comunicação';
                                    break;
                            }
                            
                        }
                        $categoria = array(
                            'key' => 'categoria',
                            'value' => $categoria,
                            'compare' => '=',
                            );
                    }
                    if ($tipo == 0){
                        $tipo = '';
                    } else {
                        $tipo =	array(
                            'key' => 'tipo',
                            'value' => $tipo,
                            'compare' => '=',
                            );
                    }
                    if ($marca == 0){
                        $marca = '';
                    } else {
                        $marca = array(
                            'key' => 'marca',
                            'value' => $marca,
                            'compare' => '=',
                            );
                    }
                }
                
                $args = array(
                    'post_type' => 'produto',
                    'posts_per_page' => 12,
                    'paged' => $paged,
                    'meta_query' => array(
                        'relation' => 'AND',
                        $tipo,
                        $marca,
                        $categoria,
                        ),
                    's' => $nome
                    );
                
                $loop = new WP_Query( $args );
                while ( $loop->have_posts()) : $loop->the_post(); ?>
                    <li class="produto">
                        <a target="_blank" href="<?php the_field('manual') ?>">
                            <figure>
                                <img class="img-responsive" src="<?php the_field('imagem') ?>">
                                <span class="ver_manual"><?php _e('Ver Catálogo', 'minamaze'); ?></span>
                            </figure>
                        </a>
                        <span class="categoria"><?php echo __( get_the_title(get_post_meta(get_the_ID(), 'tipo', true)), 'minamaze');?></span>
                        <h3><a target="_blank" href="<?php the_field('manual') ?>"><?php the_title(); ?></a></h3>
                        <p class="marca"><?php echo get_the_title(get_post_meta(get_the_ID(), 'marca', true)) ?></p>
                    </li>
                <?php endwhile; ?>
            </ul>
            
            <div style="text-align: center;">
                <?php wp_pagenavi(array( 'query' => $loop )); ?>
            </div>
            
            <section class="medium container links-produtos" style="display: none;">
                <a id="quem_somos_onde_estamos" class="pagelink leftlink" style="background: #fff;">
                    <img class="img-responsive" src='<?php echo get_template_directory_uri(); ?>/img/pagelink-ondeestamos.png' alt="Onde estamos">
                    <h3><?php _e('ONDE', 'minamaze'); ?><br><?php _e('ESTAMOS', 'minamaze'); ?></h3>
                </a>
                <a id="quem_somos_o_que_fazemos" class="pagelink rightlink" style="background: #fff;">
                    <img class="img-responsive" src='<?php echo get_template_directory_uri(); ?>/img/pagelink-oquefazemos.png' alt="O que fazemos">
                    <h3><?php _e('O QUE', 'minamaze'); ?><br><?php _e('FAZEMOS', 'minamaze'); ?></h3>
                </a>
            </section>
            
        </div>
        
    </article>
    
</main>

<?php get_footer(); ?>