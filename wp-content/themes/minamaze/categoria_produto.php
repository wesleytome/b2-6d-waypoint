<?php $curLanguage = substr(get_bloginfo( 'language' ), 0, 2); ?>

<script type="text/javascript">
    /* Verifica se e mobile */
    var isMobile = true;
    if (/(Android|BlackBerry|iPhone|iPod|iPad|Palm|Symbian)/.test(navigator.userAgent))
        isMobile = true;
    
    jQuery(window).load(function() {
        
        console.log(jQuery(window).width());
        
        if ( isMobile && jQuery(window).width() < 464 ) {
            var curLang = "<?php echo $curLanguage; ?>";
            if (curLang === 'en') {
                if ( jQuery(window).width() > 304 ) {
                    jQuery( '#home_produto_comunicacao h3' ).addClass( 'title-communication-480' );
                } else {
                    jQuery( '#home_produto_comunicacao h3' ).addClass( 'title-communication' );
                }
            } else if (curLang === 'es') {
                jQuery( '#home_produto_comunicacao h3' ).addClass( 'title-comunicacion' );
            } else {
                jQuery( '#home_produto_comunicacao h3' ).addClass( 'title-comunicacao' );
            }
        }
    });
</script>

<section class="container nossosprodutos">
    <h2><a id="home_produto"><?php _e('NOSSOS PRODUTOS', 'minamaze'); ?></a></h2>
    <ul>
        <li id="home_produto_comunicacao" class="comunicacao" style="cursor:pointer;">
            <h3><?php _e('Comunicação', 'minamaze'); ?></h3>
        </li>
        <li id="home_produto_navegacao" class="navegacao" style="cursor:pointer;">
            <h3><?php _e('Navegação', 'minamaze'); ?></h3>
        </li>
        <li id="home_produto_seguranca" class="seguranca" style="cursor:pointer;">
            <h3><?php _e('Segurança', 'minamaze'); ?></h3>
        </li>
    </ul>
</section>