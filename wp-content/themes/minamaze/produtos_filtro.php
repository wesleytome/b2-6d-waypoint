<section class="filtros container">
	<form  id="formSearchProdutos" action="">
		<select name="dropCategoria" id="drpProdutoCategoria">
			<option value="0"><?php _e('Todas as categorias', 'minamaze'); ?></option>
			<option value="1"><?php _e('Comunicação', 'minamaze'); ?></option>
			<option value="2"><?php _e('Navegação', 'minamaze'); ?></option>
			<option value="3"><?php _e('Segurança', 'minamaze'); ?></option>
		</select>
                
		<select name="drpTipo" id="drpProdutoTipo">
			<option value="0"><?php _e('Todos os tipos', 'minamaze'); ?></option>;
		<?php $loop = new WP_Query( array( 'post_type' => 'tipo_produto', 'posts_per_page' => 100) );
		while ( $loop->have_posts()) : $loop->the_post(); ?>
			<option value="<?php echo get_the_ID()?>">				
				<?php
					global $contact_form_Id;
					global $contact_form_Id_us;
                                        
					if (isset($_GET['lang'])) {
                                            if ($_GET['lang'] == 'en') {
                                                echo the_field('tipoProduto_us');
                                            } else if ($_GET['lang'] == 'es') {
                                                echo the_field('tipoProduto_es');
                                            }
					} else {
                                            echo the_title();
					}
				?>
			</option>
		<?php endwhile; ?>
		</select>
		<select name="drpMarca" id="drpProdutoMarca">
			<option value="0"><?php _e('Todas as marcas', 'minamaze'); ?></option>;
		<?php $loop = new WP_Query( array( 'post_type' => 'marca_produto', 'posts_per_page' => 100) );
		while ( $loop->have_posts()) : $loop->the_post(); ?>
			<option value="<?php echo get_the_ID()?>"><?php the_title(); ?></option>
		<?php endwhile; ?>
		</select>
		<span>
			<input type="button" value="<?php _e('Limpar Filtros', 'minamaze'); ?>" id="btnCleanProduto">
		</span>
		<input type="text" name="nome" id="txtNome" placeholder="<?php _e('Buscar por nome do produto', 'minamaze'); ?>">
		<input type="button" value="<?php _e('Buscar', 'minamaze'); ?>" id="btnBuscarProduto">
	</form>
</section>