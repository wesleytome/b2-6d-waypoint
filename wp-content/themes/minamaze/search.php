<?php
/**
Template Name: Pesquisa
* The template for displaying Search Results pages.
*
* @package ThinkUpThemes
*/

get_header(); ?>

<main class="busca interna">
    
    <article class="busca container">
        <?php if ( have_posts() ) :
        $busca = htmlspecialchars($_GET["s"]);
        $loop = new WP_Query( array( 'post_type' => 'post', 's' => $busca ) );
        
        echo '<h2><span class="numero_de_resultados">' . $loop->found_posts . '</span>  '. __('resultado para ', 'minamaze') . '"' . $busca . '"</h2>';
        echo '<ul style="list-style:none;">';
            while ( $loop->have_posts()) : $loop->the_post(); ?>
                <li class="resultado">
                    <?php if (isset($_GET['lang']) && $_GET['lang'] == 'en') { ?>
                        <a href="novidade/?lang=en&post_id=<?php echo get_the_ID() ?>">
                    <?php } else if (isset($_GET['lang']) && $_GET['lang'] == 'es') { ?>
                        <a href="novidade/?lang=es&post_id=<?php echo get_the_ID() ?>">
                    <?php } else { ?>
                        <a href="novidade/?post_id=<?php echo get_the_ID() ?>">
                    <?php } ?>
                            <figure>
                                <?php the_post_thumbnail(array(300, 250)) ?>
                            </figure>
                            <h3><?php /*$id = get_the_ID();*/ the_title(); ?></h3>
                        </a>
                        <p><?php the_excerpt(); ?></p>
                </li>
            <?php endwhile;  ?>
            </ul>
	<?php else : ?>
            <?php get_template_part( 'no-results', 'search' ); ?>
	<?php endif; ?>
    </article>
    
</main>

<?php get_footer(); ?>