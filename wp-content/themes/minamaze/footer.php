<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id="main-core".
 *
 * @package ThinkUpThemes
 */
?>

        <?php /* Sidebar */ thinkup_sidebar_html(); ?>
        <script src="<?php echo get_template_directory_uri(); ?>/js/script.js"></script>
        <footer>
            <div class="row">
                <?php if ( has_nav_menu( 'sub_footer_menu' ) ) : ?>
                <div class="menu-footer col-md-12">
                    <?php wp_nav_menu( array( 'depth' => 1, 'container_class' => 'sub-footer-links', 'container_id' => 'footer-menu', 'theme_location' => 'sub_footer_menu' ) ); ?>
                </div>
                <?php endif; ?>
                <div class="subfooter col-md-12">
                    <?php
                    global $contact_form_Id;
                    global $contact_form_Id_us;

                    if (isset($_GET['lang'])) {
                        echo "1310 Park Central Blvd South - FL 33064 | Tel: +1 954 648 7618";
                    } else {
                        echo "Av. Venezuela 3, 18º andar - Centro - Rio de Janeiro/RJ - Telefone: +55 21 2203-9000";
                    }
                    ?>
                </div>
            </div>
        </footer><!-- footer -->
    </div>
    <?php wp_footer(); ?>
</body>
</html>