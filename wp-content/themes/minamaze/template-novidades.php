<?php
/*
Template Name: Lista Novidades
*/
?>

<?php get_header(); ?>

<main class="novidades interna">

    <section class="titulo" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/header-novidades.png');">
        <div class="medium container">
            <div class="title-box">
                <h1><?php _e('Notícias', 'minamaze'); ?></h1>
            </div>
        </div>
    </section>

    <article class="novidades container">
        <ul>
            <?php
            $paged = get_query_var('paged') ? get_query_var('paged') : 1;

            $loop = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => 8, 'paged' => $paged ) );
            while ( $loop->have_posts()) : $loop->the_post(); ?>
                <li class="novidade">
                    <?php if (isset($_GET['lang']) && $_GET['lang'] == 'en') { ?>
                        <a href="novidade/?lang=en&post_id=<?php echo get_the_ID() ?>">
                    <?php } else if (isset($_GET['lang']) && $_GET['lang'] == 'es') { ?>
                        <a href="novidade/?lang=es&post_id=<?php echo get_the_ID() ?>">
                    <?php } else { ?>
                        <a href="novidade/?post_id=<?php echo get_the_ID() ?>">
                    <?php } ?>
                            <figure><?php the_post_thumbnail(array(300,250)) ?></figure>
                            <h3><?php the_title()?></h3>
                            <p><?php the_excerpt()?></p>
                        </a>
                </li>
            <?php endwhile;  ?>

            <li class="newsletter">
                <h3><?php _e('Receba todas as novidades por email', 'minamaze'); ?></h3>
                <form action="">
                    <?php
                    global $contact_news_Id;
                    global $contact_news_Id_us;
                    global $contact_news_Id_es;

                    if (isset($_GET['lang']) && $_GET['lang'] == 'en') {
                        echo do_shortcode("[contact-form-7 id='" . $contact_news_Id_us . " ' title='NewsLetters']");
                    } else if (isset($_GET['lang']) && $_GET['lang'] == 'es') {
                        echo do_shortcode("[contact-form-7 id='" . $contact_news_Id_es . "' title='Novedades']");
                    } else {
                        echo do_shortcode("[contact-form-7 id='" . $contact_news_Id . "' title='Novidades']");
                    }
                    ?>
                </form>
            </li>
        </ul>

        <div style="text-align:center;">
            <?php wp_pagenavi(array( 'query' => $loop )); ?>
        </div>

    </article>

</main>

<?php get_footer(); ?>