<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package ThinkUpThemes
 */

get_header(); ?>

<main class="novidades interna">				
	<article class="novidades container">
		<p>
			<h3>
				<?php _e('Não há resultado para a sua busca', 'minamaze'); ?>.
			</br>
				<?php _e('Por favor, entre em contato', 'minamaze'); ?>
			</h3>
		</p>
		<?php
                global $contact_form_Id;
                global $contact_form_Id_us;
                global $contact_form_Id_es;

                if (isset($_GET['lang']) && $_GET['lang'] == 'en') {
                    echo do_shortcode("[contact-form-7 id='" . $contact_form_Id_us . " ' title='Contact Form']");
                } else if (isset($_GET['lang']) && $_GET['lang'] == 'es') {
                    echo do_shortcode("[contact-form-7 id='" . $contact_form_Id_es . " ' title='Formulario de contcato']");
                } else {
                    echo do_shortcode("[contact-form-7 id='" . $contact_form_Id . "' title='Formulário de contato']");
                }
                ?>
	</article>
</main>

<?php get_footer(); ?>