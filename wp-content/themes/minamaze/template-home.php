<?php
/*
Template Name: Home
*/
?>

<?php get_header(); ?>

<?php while( have_posts() ): the_post(); ?>
    <section class="carousel">
        <div class="carousel_stage">
            
            <div class="row">
                <div class="carousel_conteudo col-md-12">
            
            <?php
            $mykey_values = get_group( 'banner_de_destaque' );
            foreach ( $mykey_values as $key => $value ) {
                echo '<div class="slide">';
                echo '<img src="' . get('banner_de_destaque_imagem', $key) . '" alt="' . get('banner_de_destaque_titulo', $key) . '"/></br>';                    
                echo '</div>';
            }
            ?>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="carousel_nav">
                <ul>
                    <?php
                    
                    if (isset($_GET['lang']) && $_GET['lang'] == 'en') {
                        $param = '?lang=en&post_id=';
                    } else if (isset($_GET['lang']) && $_GET['lang'] == 'es') {
                        $param = '?lang=es&post_id=';
                    } else {
                        $param = '?post_id=';
                    }
                    
                    $mykey_values = get_group( 'banner_de_destaque' );
                    foreach ( $mykey_values as $key => $value ) {
                        
                        $link_destaque = get('banner_de_destaque_link', $key);
                        $link_full = str_replace('?post_id=', $param, $link_destaque);
                        
                        echo '<li class="slide">';
                        echo '<a href="' . $link_full . '" target="_blank" style="text-decoration:none;">';
                        
                        if (isset($_GET['lang']) && $_GET['lang'] == 'en') {
                            $titulo = get('banner_de_destaque_titulo_us', $key);
                            $resumo = get('banner_de_destaque_resumo_us', $key);
                            
                        } else if (isset($_GET['lang']) && $_GET['lang'] == 'es') {
                            $titulo = get('banner_de_destaque_titulo_es', $key);
                            $resumo = get('banner_de_destaque_resumo_es', $key);
                            
                        } else{              
                            $titulo = get('banner_de_destaque_titulo', $key);
                            $resumo = get('banner_de_destaque_resumo', $key);
                        }
                        echo '<h3>' . $titulo . '</h3>';
                        echo '</a>';
                        echo '<p>' . $resumo . '</p>';
                        echo '</li>';
                    } ?>
                </ul>
                <p class="pagination"></p>
            </div>
        </div>
    </section>
<main class="home">
	<?php get_template_part( 'categoria_produto' ); ?>
  <div class="container">
    <?php
        $image = get_field('imagem_de_destaque');
    ?>
    <figure class="banner">
        <h3><?php _e('Navegue com segurança', 'minamaze'); ?></h3>
        <img src="<?php echo  $image['url']  ?>" alt="<?php echo  $image['caption']  ?>">
    </figure>
  </div>
  <section class="projetos">
    <div class="container lista">
      <h2><a id="home_projeto"><?php _e('PROJETOS', 'minamaze'); ?></a></h2>
      <?php get_template_part( 'projeto' ); ?>
      <a id="btn_home_projeto" class="botaoProjeto"><?php _e('Projetos', 'minamaze'); ?></a>
    </div>
  </section>
  <?php listagem_parceiros() ?>
</main>
<?php endwhile;  ?>

<?php get_footer(); ?>

