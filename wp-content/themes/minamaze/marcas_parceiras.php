<section id="marcas_parceiras">
    <div class="container">
        <h2>Marcas parceiras</h2>
        <ul class="parceiras_carousel">
			<?php
            $loop = new WP_Query( array( 'post_type' => 'parceiro' ) );
            while ( $loop->have_posts()) : $loop->the_post(); ?>
                <li class="parceiras_slide" style="display: -webkit-box;">
                    <img style="border-radius:50%; max-height:140px; max-width:140px;" src="<?php the_field('imagem') ?>">
                </li>
            <?php endwhile;  ?>
        </ul>
    </div>
</section>