<?php
/*
Template Name: Novidade
*/
?>

<?php get_header(); ?>

<main class="novidade interna">

    <section class="titulo" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/header-novidades.png');">
        <div class="medium container">
            <div class="title-box">
                <h1><?php _e('Notícias', 'minamaze'); ?></h1>
            </div>
        </div>
    </section>

    <div class="container" style="overflow: hidden;">

        <article class="novidade" style="border-bottom-width: 0px; ">
            <?php
            $post_id = htmlspecialchars($_GET["post_id"]);
            $post = get_post($post_id);
            setup_postdata($post);
            ?>
            <h1><?php the_title(); ?></h1>
            <?php the_post_thumbnail() ?>
            <p><?php the_content(); ?></p>
            <div class='formulario' style="border-top: solid 1px #d8d8d8; padding-top: 25px;">
                <h2 style="text-align: left;"><?php _e('ENTRE EM CONTATO', 'minamaze'); ?></h2>
                    <?php
                    global $contact_form_Id;
                    global $contact_form_Id_us;
                    global $contact_form_Id_es;

                    if (isset($_GET['lang']) && $_GET['lang'] == 'en') {
                        echo do_shortcode("[contact-form-7 id='" . $contact_form_Id_us . " ' title='Contact Form']");
                    } else if (isset($_GET['lang']) && $_GET['lang'] == 'es') {
                        echo do_shortcode("[contact-form-7 id='" . $contact_form_Id_es . " ' title='Formulario de contcato']");
                    } else {
                        echo do_shortcode("[contact-form-7 id='" . $contact_form_Id . "' title='Formulário de contato']");
                    }
                    ?>
            </div>
        </article>

        <aside class="recentes">
            <div class="newsletter">
                <h3><?php _e('Receba todas as novidades por email', 'minamaze'); ?></h3>
                <form action="">
                    <?php
                    global $contact_news_Id;
                    global $contact_news_Id_us;
                    global $contact_news_Id_es;

                    if (isset($_GET['lang']) && $_GET['lang'] == 'en') {
                        echo do_shortcode("[contact-form-7 id='" . $contact_news_Id_us . " ' title='NewsLetters']");
                    } else if (isset($_GET['lang']) && $_GET['lang'] == 'es') {
                        echo do_shortcode("[contact-form-7 id='" . $contact_news_Id_es . "' title='Novedades']");
                    } else {
                        echo do_shortcode("[contact-form-7 id='" . $contact_news_Id . "' title='Novidades']");
                    }
                    ?>
                </form>
            </div>
            <ul>
            <?php
            $paged = get_query_var('paged') ? get_query_var('paged') : 1;
            $loop = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => 3, 'paged' => $paged, 'order' => 'ASC' ) );

            while ( $loop->have_posts()) : $loop->the_post(); ?>
                <?php if (get_the_ID() != $post_id) : ?>
                    <li class="recente">
                        <?php if (isset($_GET['lang']) && $_GET['lang'] == 'en') { ?>
                            <a href="novidade/?lang=en&post_id=<?php echo get_the_ID() ?>">
                        <?php } else if (isset($_GET['lang']) && $_GET['lang'] == 'es') { ?>
                            <a href="novidade/?lang=es&post_id=<?php echo get_the_ID() ?>">
                        <?php } else { ?>
                            <a href="novidade/?post_id=<?php echo get_the_ID() ?>">
                        <?php } ?>
                        <figure><?php the_post_thumbnail(array(300,250)) ?></figure>
                        <h3><?php the_title()?></h3>
                        </a>
                        <p><?php the_excerpt()?></p>
                    </li>
                <?php endif; ?>
            <?php endwhile;  ?>
            </ul>
        </aside>
    </div>
</main>

<?php get_footer(); ?>