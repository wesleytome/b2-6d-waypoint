<?php
/*
Template Name: Quem Somos
*/
?>

<?php get_header();?>
    <?php while( have_posts() ): the_post(); ?>
        <main class="quemsomos interna">
            
            <section class="titulo" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/header-quemsomos.png');">
                <div class="medium container">
                    <div class="title-box">
                        <h1><?php _e('QUEM SOMOS', 'minamaze'); ?></h1>
                    </div>
                </div>
            </section>
            
            <article class="quemsomos">
                
                <section class="medium container">
                    <p><?php the_field('quem_somos'); ?></p>
                </section>
                
                <section class="graybg">
                    <div class="medium container">
                        <div class="container mini pull-left">
                            <h3><?php _e('NOSSA POLÍTICA', 'minamaze'); ?></h3>
                            <p><?php the_field('missão'); ?></p>
                        </div>
                        <div class="medium container mini pull-right">
                            <h3><?php _e('NOSSOS OBJETIVOS', 'minamaze'); ?></h3>
                            <p><?php the_field('visão'); ?></p>
                        </div>
                    </div>
                </section>
                
                <section class="medium container">
                    <a id="quem_somos_onde_estamos" class="pagelink leftlink" style="background: #fff;">
                        <img class="img-responsive" src='<?php echo get_template_directory_uri(); ?>/img/pagelink-ondeestamos.png' alt="Onde estamos">
                        <h3><?php _e('ONDE', 'minamaze'); ?><br><?php _e('ESTAMOS', 'minamaze'); ?></h3>
                    </a>
                    <a id="quem_somos_o_que_fazemos" class="pagelink rightlink" style="background: #fff;">
                        <img class="img-responsive" src='<?php echo get_template_directory_uri(); ?>/img/pagelink-oquefazemos.png' alt="O que fazemos">
                        <h3><?php _e('O QUE', 'minamaze'); ?><br><?php _e('FAZEMOS', 'minamaze'); ?></h3>
                    </a>
                </section>
                
                <?php get_template_part('categoria_produto'); ?>
                
            </article>
        </main>
    <?php endwhile; ?>
<?php get_footer(); ?>