function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
var _lang = getParameterByName('lang');

console.log('LANG LANG:' + _lang);


var docheight = $(document).height();
var screenheight = $(window).height();
var listheight = $('.news_tooltip .lista').height();
var newspos = screenheight / 2 - 45;
var tooltipaberta = false;

function close_tooltip() {
	event.preventDefault();
	$('#page').animate({opacity:'1'});
	$('#page').css('pointer-events', 'initial');
	$('.news_tooltip .lista').animate({right:'-260px'}, function(){
		$('#close_tooltip').hide();
		$('#news_button').animate({right:'0'});
		$('body').css('background-color', 'initial');
		tooltipaberta = false;
	});
}

$('#news_button').on('click', function(event) {
	event.preventDefault();
	var fromtop = $(window).scrollTop();
	$('body').css('background-color', '#000');

	if (fromtop > 1600) {
		fromtop = 1500;
	};
	$(this).animate({right:'-100px'}, function(){
		$('.news_tooltip .lista').css('top', fromtop + 34);
		$('.news_tooltip .lista').animate({right:'0'}, function(){
			$('#close_tooltip').fadeIn();
			tooltipaberta = true;
		});
		$('#page').animate({opacity:'0.5'});
	});
});
$('#close_tooltip').on('click', function(event) {
	close_tooltip();
});
$('#page').stop().on('click', '*', function(event) {
	if (tooltipaberta) {
		event.preventDefault();
		event.stopPropagation();
		close_tooltip();
	};
});
// Abre / fecha menu
var menuisopen = false;
$('#menu-button').on('click', function(event) {
	event.preventDefault();
	var width = $(window).width();
	var deslocamento = '260px';
//
//	if (width > 480) {
//		deslocamento = '300px';
//	} else if (width < 480) {
//		deslocamento = '85%';
//	}
	if (menuisopen) {
		$('#page').animate({left:'0'});
		$('#menu-button').animate({opacity:'0'}, function(){
			$(this).attr('class', 'open');
			$(this).animate({opacity:'1'});
		});
		menuisopen = false;
	} else {
		$('#page').animate({left: deslocamento});
		$('#menu-button').animate({opacity:'0'}, function(){
			$(this).attr('class', 'close');
			$(this).animate({opacity:'1'});
		});
		menuisopen = true;
	}
});

$(document).ready(function() {
	$('#news_button').css({
		display: 'block',
		top: newspos,
	});

	var $status = $('.pagination');
	var $slickElement = $('.carousel_stage');

	$slickElement.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
	    //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
	    var i = (currentSlide ? currentSlide : 0) + 1;
	    $status.text(i + '/' + slick.slideCount);
	});

	// Carrrossel principal
	$('.carousel_conteudo').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		asNavFor: '.carousel_nav',
	});
	$('.carousel_nav ul').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		asNavFor: '.carousel_conteudo',
		responsive: [
			{
			breakpoint: 481,
				settings: {
                                        arrows: false,
                                        dots: true
				}
			},
			{
			breakpoint: 321,
				settings: {
                                        arrows: false,
                                        dots: true
				}
			},
		]
	});

	// Carrossel de Marcas Parceiras
	$('.parceiras_carousel').slick({
		slidesToShow: 4,
		slidesToScroll: 4,
		infinite: true,
		autoplay: true,
		autoplaySpeed: 5000,
		responsive: [
			{
			breakpoint: 769,
				settings: {
                                        arrows: false,
					slidesToShow: 4,
					slidesToScroll: 4,
                                        infinite: true,
                                        dots: true
				}
			},
			{
			breakpoint: 641,
				settings: {
                                        arrows: false,
					slidesToShow: 3,
					slidesToScroll: 3,
                                        infinite: true,
                                        dots: true
				}
			},
			{
			breakpoint: 551,
				settings: {
                                        arrows: false,
					slidesToShow: 3,
					slidesToScroll: 3,
                                        infinite: true,
                                        dots: true
				}
			},
			{
			breakpoint: 481,
				settings: {
                                        arrows: false,
					slidesToShow: 3,
					slidesToScroll: 3,
                                        infinite: true,
                                        dots: true
				}
			},
			{
			breakpoint: 451,
				settings: {
                                        arrows: false,
					slidesToShow: 2,
					slidesToScroll: 2,
                                        infinite: true,
                                        dots: true
				}
			},
		]
	});
        
//        $('.parceiras_carousel').on('beforeChange', function(event, slick, currentSlide, nextSlide){
//            var currentSlide = $('.parceiras_carousel').slick('slickCurrentSlide');
            $('.parceiras_carousel ul li').each(function(i){
//                if ($(this).hasClass('slick-active')) {
//                    console.log(this);
                    $(this).find('button').addClass( 'slick-inactive' );
//                }
            });
            
//            if ($('ul.slick-dots').find('li').hasClass('slick-active')) {
//                console.log(this);
//                $( this ).find('button').addClass( 'slick-inactive' );
//            }
//        });
        
	/** Final Carrosel **/

	$.urlParam = function(name){
		var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
		return results[1] || 0;
	}

	/** Functions de Produto **/
	//Produtos - carregar os filtros.
	if ($( "#drpProdutoCategoria option:selected" ).val() == '0'){
		var str = window.location.href;

		$('#formSearchProdutos').submit(function() {
		  return false;
		});

		if (str.indexOf("dropCategoria") >= 0)
		{

			if (str.indexOf("cat_id") >= 0){
				var categoria = $.urlParam('cat_id');
				if (categoria != '') {
					$('#drpProdutoCategoria').val(categoria);
				}
			}

			if (str.indexOf("drpMarca") >= 0){
				var marca = $.urlParam('drpMarca');
				if (marca != '') {
					$('#drpProdutoMarca').val(marca);
				}
			}

			if (str.indexOf("drpTipo") >= 0){
				var tipo = $.urlParam('drpTipo');
				if (tipo != '') {
					$('#drpProdutoTipo').val(tipo);
				}
			}

			if (str.indexOf("nome") >= 0){
				var nome = $.urlParam('nome');
				if (nome != '') {
					$('#txtNome').val(nome);
				}
			}
		}
	}

	$.searchProducts = function(){
		var str = window.location.href;

		var categoria = $( "#drpProdutoCategoria option:selected").text();
		var tipo = $( "#drpProdutoTipo option:selected" ).val();
		var marca = $( "#drpProdutoMarca option:selected" ).val();
		var nome = $( "#txtNome" ).val();
		var cat_id =$( "#drpProdutoCategoria option:selected").val();

		if (str.indexOf("lang") >= 0) {
			url = base_url + '/produto/?lang=' + _lang;
			if (categoria != ''){url += '&dropCategoria=' + categoria;}
			if (tipo != ''){url += '&drpTipo=' + tipo;}
			if (marca != ''){url += '&drpMarca=' + marca;}
			if (nome != ''){url += 'nome=' + nome;}
			if (cat_id != ''){url += '&cat_id=' + cat_id;}
		}
		else{
			url = base_url;
			if (categoria != ''){url += '/produto/?dropCategoria=' + categoria;}
			if (tipo != ''){url += '&drpTipo=' + tipo;}
			if (marca != ''){url += '&drpMarca=' + marca;}
			if (nome != ''){url += '&nome=' + nome;}
			if (cat_id != ''){url += '&cat_id=' + cat_id;}
		}

        $( location ).attr("href", url);
	}


	$('#drpProdutoCategoria').on('change', function () {
		$.searchProducts();
	})

	$('#drpProdutoTipo').on('change', function () {
		$.searchProducts();
	})

	$('#drpProdutoMarca').on('change', function () {
		$.searchProducts();
	})

	$('#btnBuscarProduto').on('click', function () {
		$.searchProducts();
	})

	$('#btnCleanProduto').on('click', function () {
		var host = window.location.host;
		var str = window.location.href;		

		if (str.indexOf("lang") >= 0) {			
			url = "http://" + host + "/waypoint/produto/" + "?lang="+_lang;
		}
		else{
			url = "http://" + host + "/waypoint/produto";
		}
 		
        $( location ).attr("href", url);		
	})

	 $('#txtNome').on('keypress', function () {
		if (event.which == 13) {
		   $.searchProducts();
		}
	});
	/* Fim Function Produto */


    /* Functions de Projetos */
	$('#drpProjetoOrdernar').on('change', function () {
		var str = window.location.href;

		var cliente = $( "#drpProjetoCliente option:selected" ).val();
		var ordernar = $( "#drpProjetoOrdernar option:selected" ).val();


		if (str.indexOf("lang") >= 0) {
			url = base_url + "/projeto/?cliente=" + cliente + "&ordernar=" + ordernar + "&lang="+_lang;			
		}
		else{
			url = base_url + "/projeto/?cliente=" + cliente + "&ordernar=" + ordernar;
		}
		
       $( location ).attr("href", url);
	})

	$('#drpProjetoCliente').on('change', function () {
		var str = window.location.href;

		var cliente = $( "#drpProjetoCliente option:selected" ).val();
		var ordernar = $( "#drpProjetoOrdernar option:selected" ).val();


		if (str.indexOf("lang") >= 0) {
			url = base_url + "/projeto/?cliente=" + cliente + "&ordernar=" + ordernar + "&lang="+_lang;			
		}
		else{
			url = base_url + "/projeto/?cliente=" + cliente + "&ordernar=" + ordernar;			
		}

       $( location ).attr("href", url);
	})

	if ($( "#drpProjetoCliente option:selected" ).val() == '0'){
		var str = window.location.href;
		if (str.indexOf("cliente") >= 0)
		{
			var cliente = $.urlParam('cliente');
			var ordernar = $.urlParam('ordernar');

			if (cliente != ''){
				$('#drpProjetoCliente').val(cliente);
			}

			if (ordernar != ''){
				$('#drpProjetoOrdernar').val(ordernar);
			}
		}
	} 
	/* Fim Functions de Projetos */

	$('#btnBuscar').click(function () {
		console.log('teste');
		var host = window.location.host;
		var busca = $("txtBuscar").val();
		url =  base_url + "/search/?s=" + busca;
		$( location ).attr("href", url);
	});

	/***  Redirecionamentos 
		Mantem o idioma escolhido
	***/

	// $.RedirectIdioma = function(url){

	// 	var str = window.location.href;

	// 	if (str.indexOf("lang") >= 0) {
	// 		if (url.indexOf("&") >= 0) {				
	// 			var idioma = "&lang=en";				
	// 		}				
	// 		else{
	// 			var idioma = "?lang=en";
	// 		}
	// 		url = base_url + idioma;
	// 	}

	// 	$( location ).attr("href", url);
	// }
	$.RedirectIdioma = function(url){

		var host = window.location.host;
		var str = window.location.href;		

		if (url.indexOf("waypoint") < 0)
			url = "waypoint/" + url;

		if (str.indexOf("lang") >= 0) {
			if (url.indexOf("&") >= 0) {				
				var idioma = "&lang="+_lang;				
			}				
			else{
				var idioma = "/?lang="+_lang;
			}
			
			url = "http://" + host + "/" + url + idioma; /* em producao trocar "/works/" por "/" */
		}
		else
			url = "http://" + host + "/" + url; /* em producao trocar "/works/" por "/" */

		$( location ).attr("href", url);
	}

	$('#home_logo').on('click', function () {		
		$.RedirectIdioma('home');
	});

	/* Menu */
	$('#liMenu_home').on('click', function () {		
		$.RedirectIdioma('home');
	});

	$('#liMenu_quem_somos').on('click', function () {
		$.RedirectIdioma('quem-somos');
	});

	$('#liMenu_onde_estamos').on('click', function () {
		$.RedirectIdioma('onde-estamos');
	});

	$('#liMenu_o_que_fazemos').on('click', function () {
		$.RedirectIdioma('o-que-fazemos');
	});

	$('#liMenu_projeto').on('click', function () {
		$.RedirectIdioma('projeto');
	});

	$('#liMenu_produto').on('click', function () {
		$.RedirectIdioma('produto');
	});

	$('#liMenu_produto_comunicacao').on('click', function () {
		$.RedirectIdioma('produto/?dropCategoria=comunicação&cat_id=1');
	});

	$('#liMenu_produto_navegacao').on('click', function () {
		$.RedirectIdioma('produto/?dropCategoria=navegação&cat_id=2');
	});

	$('#liMenu_produto_seguranca').on('click', function () {
		$.RedirectIdioma('produto/?dropCategoria=segurança&cat_id=3');
	});

	$('#liMenu_contato').on('click', function () {
		$.RedirectIdioma('contato');
	});

	$('#liMenu_novidades').on('click', function () {
		$.RedirectIdioma('noticias');
	});
	/* Fim Menu */

	/* Home */
	$('#home_produto').on('click', function () {
			$.RedirectIdioma('produto');
		});

	$('#home_produto_comunicacao').on('click', function () {
			$.RedirectIdioma('produto/?dropCategoria=comunicação&cat_id=1');
		});

	$('#home_produto_navegacao').on('click', function () {
			$.RedirectIdioma('produto/?dropCategoria=navegação&cat_id=2');
		});

	$('#home_produto_seguranca').on('click', function () {
			$.RedirectIdioma('produto/?dropCategoria=segurança&cat_id=3');
		});

	$('#home_projeto').on('click', function () {
			$.RedirectIdioma('projeto');
		});

	$('#btn_home_projeto').on('click', function () {
			$.RedirectIdioma('projeto');
		});
	/* Fim Home */

	/* Pagina Quem Somos */
	$('#quem_somos_o_que_fazemos').on('click', function () {
		$.RedirectIdioma('o-que-fazemos');
	});

	$('#quem_somos_onde_estamos').on('click', function () {
		$.RedirectIdioma('onde-estamos');
	});

	/* Pagina Aonde estamos*/
	$('#onde_estamos_quem_somos').on('click', function () {
		$.RedirectIdioma('quem-somos');
	});

	$('#onde_estamos_o_que_fazemos').on('click', function () {
		$.RedirectIdioma('o-que-fazemos');
	});

	/* Pagina o que fazemos*/
	$('#o_que_fazemos_quem_somos').on('click', function () {
		$.RedirectIdioma('quem-somos');
	});

	$('#o_que_fazemos_onde_estamos').on('click', function () {
		$.RedirectIdioma('onde-estamos');
	});	

	$('#o_que_fazemos_btn_projetos').on('click', function () {
		$.RedirectIdioma('projeto');
	});	

	$('#o_que_fazemos_projetos').on('click', function () {
		$.RedirectIdioma('projeto');
	});	

	

	

	/*** Fim Redirecionamentos***/
});