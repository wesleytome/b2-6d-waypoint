<?php
/**
 * Template Name: Contato
 */

get_header(); ?>
    <main class="busca interna">
        
        <section class="titulo" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/header-contato.png');">
            <div class="medium container">
                <div class="title-box">
                    <h1><?php _e('ENTRE EM CONTATO', 'minamaze'); ?></h1>
                </div>
            </div>
        </section>
        
        <article class="contato container">
            <div class="formulario mini container">
                <?php
                global $contact_form_Id;
                global $contact_form_Id_us;
                global $contact_form_Id_es;
                
                if (isset($_GET['lang']) && $_GET['lang'] == 'en') {
                    echo do_shortcode("[contact-form-7 id='" . $contact_form_Id_us . " ' title='Contact Form']");
                } else if (isset($_GET['lang']) && $_GET['lang'] == 'es') {
                    echo do_shortcode("[contact-form-7 id='" . $contact_form_Id_es . " ' title='Formulario de contcato']");
                } else {
                    echo do_shortcode("[contact-form-7 id='" . $contact_form_Id . "' title='Formulário de contato']");
                }
                ?>
            </div>
            
            <div class="local mini container bg-localizacao">
                <h3 class="mobile-title" style="display: none;">Localização</h3>
                <div class="map-placeholder">
                    <?php echo do_shortcode("[put_wpgm id=1]"); ?>
                </div>
                <?php
                if (isset($_GET['lang'])) { ?>
                    <h4>EUA - Florida</h4>
                    <p><?php the_field('localização_nos_eua'); ?></p>
                    <h4>Brasil - RJ</h4>
                    <p><?php the_field('localização_no_brasil_rj'); ?></p>
                    <h4>Brasil - AM</h4>
                    <p><?php the_field('localização_no_brasil_ma'); ?></p>
                <?php } else { ?>
                    <h4>Brasil - RJ</h4>
                    <p><?php the_field('localização_no_brasil_rj'); ?></p>
                    <h4>Brasil - AM</h4>
                    <p><?php the_field('localização_no_brasil_ma'); ?></p>
                    <h4>EUA</h4>
                    <p><?php the_field('localização_nos_eua'); ?></p>
                <?php } ?>
            </div>
        </article>
    </main>
<?php get_footer(); ?>