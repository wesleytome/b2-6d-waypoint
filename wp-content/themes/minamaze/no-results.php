<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * @package ThinkUpThemes
 */
?>
<main class="novidades interna">				
	<article class="novidades container">
		<p>
			<h3>
				<?php _e('Não há resultado para a sua busca', 'minamaze'); ?>.
			</br>
				<?php _e('Por favor, entre em contato', 'minamaze'); ?>
			</h3>
		</p>		
		<div class='formulario'>
			<?php
				global $contact_form_Id;
				global $contact_form_Id_us;

				if (isset($_GET['lang'])) {				
					echo do_shortcode("[contact-form-7 id='" . $contact_form_Id_us . " ' title='Contact Form']");
				}else{
					echo do_shortcode("[contact-form-7 id='" . $contact_form_Id . "' title='Formulário de contato']");
				}
			?>		
		</div>
	</article>
</main>