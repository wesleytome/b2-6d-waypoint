<!DOCTYPE html>

<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/lib/scripts/html5.js" type="text/javascript"></script>
<![endif]-->

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/slick.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/lib/extentions/bootstrap-3.3.6-dist/js/bootstrap.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/lib/extentions/bootstrap-3.3.6-dist/css/bootstrap.min.css">

<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/styles/slick.css"/>
<!--<link rel="stylesheet" type="text/css" href="<?php //echo get_template_directory_uri(); ?>/styles/slick-theme.css"/>-->

<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/stylesheets/estilo.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/styles/style-responsive.css">

<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Bitter:400,400italic,700'>
<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700'>

<!-- ?php wp_head(); ? -->
<title>Waypoint | <?php _e('Equipamentos de Navegação', 'minamaze'); ?></title>

<meta name="description" content="A Waypoint está presente em todo território nacional e fornece equipamentos e soluções de eletrônica naval e comunicação de navegação.">


<meta property='og:title' content='Waypoint | Equipamentos de Navegação' />
<meta property='og:description' content='A Waypoint está presente em todo território nacional e fornece equipamentos e soluções de eletrônica naval e comunicação de navegação.' />
<meta property='og:url' content='http://www.waypointbrasil.com.br/' />
<meta property='og:image' content='http://www.waypointbrasil.com.br/wp-content/themes/minamaze/img/waypoint_share-image.jpg'/>
<meta property='og:type' content='website' />
<meta property='og:site_name' content='Waypoint' />

<!-- iPad iOS7+ com Retina Display -->
<link rel="apple-touch-icon" sizes="152x152" href="http://www.waypointbrasil.com.br/wp-content/themes/minamaze/img/waypoin_icon_152x152.png">
<!-- iPhone iOS7+ com Retina Display -->
<link rel="apple-touch-icon" sizes="120x120" href="http://www.waypointbrasil.com.br/wp-content/themes/minamaze/img/waypoin_icon_120x120.png">
<!-- iPad iOS7+ sem retina display e iPad Mini-->
<link rel="apple-touch-icon" sizes="76x76" href="http://www.waypointbrasil.com.br/wp-content/themes/minamaze/img/waypoin_icon_76x76.png">

<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico">


<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-66097785-1', 'auto');
    ga('send', 'pageview');

    var base_url = '<?php echo get_bloginfo("url"); ?>';
</script>



<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico">
</head>

<body style="background-color: #fff;">
    <?php if(is_home() || is_front_page()): ?>
    <div class="news_tooltip" style="display:none;">
        <button id="news_button"><?php _e('Notícias', 'minamaze'); ?></button>
        <div class="lista">
            <button id="close_tooltip"></button>
            <ul>
                <?php
                $loop = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => 3, ) );
                while ( $loop->have_posts()) : $loop->the_post(); ?>
                    <li class="novidade">
                        <?php if (isset($_GET['lang'])) { ?>
                                <a href="noticias/?lang=en&post_id=<?php echo get_the_ID() ?>">          
                            <?php }else{ ?>
                                <a href="noticias/?post_id=<?php echo get_the_ID() ?>">      
                            <?php } ?>                    
                        <?php if ( has_post_thumbnail()): ?>                        
                            <figure style="overflow:hidden;height:167px">
                                <?php the_post_thumbnail(array(300,250)) ?>
                            </figure>
                        <?php endif; ?>
                        <h4>
                            <?php $id = get_the_ID();                            
                                the_title();
                            ?>
                        </h4></a>
                        <p><?php the_excerpt(); ?></p>
                    </li>
                <?php endwhile;  ?>
                <li style="border-bottom-width: 0px;">
                    <?php if (isset($_GET['lang'])) { ?>
                        <a style="text-decoration: none;" href="noticias/?lang=en" class="allNovidades"><?php _e('Todas as novidades', 'minamaze'); ?></a>
                    <?php }else{ ?>
                        <a style="text-decoration: none;" href="noticias" class="allNovidades"><?php _e('Todas as novidades', 'minamaze'); ?></a>
                    <?php } ?>
                </li>
                 <li class="newsletter">
                    <h3><?php _e('Receba todas as novidades por email', 'minamaze'); ?></h3>
                    <form action="">
                        <?php
                            global $contact_news_Id;
                            global $contact_news_Id_us;

                            if (isset($_GET['lang'])) {             
                                echo do_shortcode("[contact-form-7 id='" . $contact_news_Id_us . " ' title='NewsLetters']");
                            }else{
                                echo do_shortcode("[contact-form-7 id='" . $contact_news_Id . "' title='Novidades']");
                            }
                        ?>      
                    </form>
                </li>
            </ul>
        </div>
    </div>
    <?php endif; ?>
    
    <div id="page" style="left:0px; background-color: #fff; ">
        <nav id="menu">
            <ul>
                <li id="liMenu_home"><a><h2>WAYPOINT</h2></a></li>
                <li id="liMenu_quem_somos"><a><?php _e('Quem somos', 'minamaze'); ?></a></li>
                <li id="liMenu_onde_estamos"><a><?php _e('Onde estamos', 'minamaze'); ?></a></li>
                <li id="liMenu_o_que_fazemos"><a><?php _e('O que fazemos', 'minamaze'); ?></a></li>
                <li id="liMenu_projeto"><a><h2><?php _e('PROJETOS', 'minamaze'); ?></h2></a></li>
                <li id="liMenu_produto"><a><h2><?php _e('PRODUTOS', 'minamaze'); ?></h2></a></li>
                <li id="liMenu_produto_comunicacao"><a><?php _e('Comunicação', 'minamaze'); ?></a></li>
                <li id="liMenu_produto_navegacao"><a><?php _e('Navegação', 'minamaze'); ?></a></li>
                <li id="liMenu_produto_seguranca"><a><?php _e('Segurança', 'minamaze'); ?></a></li>
                <li id="liMenu_contato"><a><h2><?php _e('CONTATO', 'minamaze'); ?></h2></a></li>
                <li id="liMenu_novidades"><a><h2><?php _e('NOTÍCIAS', 'minamaze'); ?></h2></a></li>
                <?php if ( is_active_sidebar('pesquisa')) : ?>
                    <li>
                        <div>
                            <?php dynamic_sidebar('pesquisa'); ?>
                        </div>
                    </li>
                <?php endif; ?>
            </ul>
        </nav>
        
        <header class="main-header">
            <div id="header">
                <div class="container">
                    <button id="menu-button" class="open">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/hamburger.png" style="width:30px;height:30px;">
                        Menu
                    </button>
                    <a id="home_logo" title="Waypoint">
                        <h1>Waypoint</h1>
                    </a>
                    <?php if ( is_active_sidebar('idiomas')) : ?>                
                        <?php dynamic_sidebar('idiomas'); ?>                
                    <?php endif; ?>
                </div>
            </div>
        </header>
        