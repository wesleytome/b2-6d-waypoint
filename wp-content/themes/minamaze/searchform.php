<?php
/**
 * The template for displaying search forms.
 *
 * @package ThinkUpThemes
 */
?>
<form method="get" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
    <?php if (isset($_GET['lang']) && $_GET['lang'] == 'en') { ?>
        <input type="text" class="search" name="lang" value="en" style="display:none;">
    <?php } else if (isset($_GET['lang']) && $_GET['lang'] == 'es') { ?>
        <input type="text" class="search" name="lang" value="es" style="display:none;">
    <?php } ?>
        <input type="text" class="search" name="s" value="<?php echo esc_attr( get_search_query() ); ?>" placeholder="<?php _e( 'Buscar', 'lan-thinkupthemes' ) . ' &hellip;'; ?>" />
	<input type="submit" class="searchsubmit" name="submit" value="Search" />
</form>