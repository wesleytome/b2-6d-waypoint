<?php
/*
Template Name: Onde Estamos
*/
?>

<?php get_header();?>
    <?php while( have_posts() ): the_post(); ?>
        <main class="ondeestamos interna">
            
            <section class="titulo" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/header-ondeestamos.png');">
                <div class="medium container">
                    <div class="title-box">
                        <h1><?php _e('ONDE ESTAMOS', 'minamaze'); ?></h1>
                    </div>
                </div>
            </section>
            
            <article class="ondeestamos">
                
                <section class="medium container">
                    <p><?php the_field('onde_estamos'); ?></p>
                </section>
                
                <section class="graybg">
                    <div class="medium container">
                        <h3><?php _e('LOCALIZAÇÃO', 'minamaze'); ?></h3>
                        <div class="map-placeholder">
                            <?php echo do_shortcode("[put_wpgm id=1]"); ?>
                        </div>
                        <?php
                        global $contact_form_Id;
                        global $contact_form_Id_us;

                        if (isset($_GET['lang'])) { ?>
                            <h4>EUA - Florida</h4>
                            <p><?php the_field('localização_nos_eua'); ?></p>
                            <h4>Brasil - RJ</h4>
                            <p><?php the_field('localização_no_brasil_rj'); ?></p>
                            <h4>Brasil - AM</h4>
                            <p><?php the_field('localização_no_brasil_ma'); ?></p>
                        <?php } else { ?>
                            <h4>Brasil - RJ</h4>
                            <p><?php the_field('localização_no_brasil_rj'); ?></p>
                            <h4>Brasil - AM</h4>
                            <p><?php the_field('localização_no_brasil_ma'); ?></p>		
                            <h4>EUA</h4>
                            <p><?php the_field('localização_nos_eua'); ?></p>
                        <?php } ?>
                    </div>
                </section>
                
                <section class="medium container">
                    <a id="onde_estamos_quem_somos" class="pagelink leftlink" style="background: #fff;">
                        <img class="img-responsive" src='<?php echo get_template_directory_uri(); ?>/img/pagelink-quemsomos.png' alt="Onde estamos">
                        <h3><?php _e('QUEM', 'minamaze'); ?><br><?php _e('SOMOS', 'minamaze'); ?></h3>
                    </a>
                    <a id="onde_estamos_o_que_fazemos" class="pagelink rightlink" style="background: #fff;">
                        <img class="img-responsive" src='<?php echo get_template_directory_uri(); ?>/img/pagelink-oquefazemos.png' alt="O que fazemos">
                        <h3><?php _e('O QUE', 'minamaze'); ?><br><?php _e('FAZEMOS', 'minamaze'); ?></h3>
                    </a>
                </section>
                
            </article>
        </main>
    <?php endwhile; ?>
<?php get_footer(); ?>